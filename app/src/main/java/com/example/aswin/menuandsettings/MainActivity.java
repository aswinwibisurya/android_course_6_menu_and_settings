package com.example.aswin.menuandsettings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView tvSwitchState;
    TextView tvLongPress;
    boolean isColorChanged;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvSwitchState = findViewById(R.id.tvSwitchState);
        tvLongPress = findViewById(R.id.tvLongPress);
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerForContextMenu(tvLongPress);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterForContextMenu(tvLongPress);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean switchState = sharedPrefs.getBoolean("switch_preference_1", false);

        if(switchState) {
            tvSwitchState.setText("Switch is On");
        } else {
            tvSwitchState.setText("Switch is Off");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_option_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_favorites:
                Toast.makeText(this, "Favorites selected", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_help:
                Toast.makeText(this, "Help selected", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if(v == tvLongPress) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.text_context_menu, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_change_color:
                changeColor();
                break;
        }
        return super.onContextItemSelected(item);
    }

    public void onButtonClick(View view) {
        PopupMenu popupMenu = new PopupMenu(this, tvLongPress);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.text_context_menu, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.menu_change_color:
                    changeColor();
                }
                return true;
            }
        });

        popupMenu.show();
    }

    private void changeColor() {
        isColorChanged = !isColorChanged;
        int color;
        if(isColorChanged)
            color = getResources().getColor(android.R.color.holo_red_dark);
        else
            color = getResources().getColor(android.R.color.holo_blue_dark);
        tvLongPress.setTextColor(color);
    }
}
